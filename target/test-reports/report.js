$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/Items.feature");
formatter.feature({
  "name": "Testing the items",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.scenario({
  "name": "Successfully adding an item",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a \"Apple MacBook Pro 16\" item is created",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.ItemsSteps.a_item_is_created(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the CPU model is \"Intel Core i9\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.the_CPU_model_is(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "has a price of \"1849.99\"",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.has_a_price_of(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request to add the item is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_add_the_item_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 200 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a \"Apple MacBook Pro 16\" is created",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.a_is_created(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Successfully retrieving an item",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "an item has been added",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.ItemsSteps.an_item_has_been_added()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request to retrieve the item is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_retrieve_the_item_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 200 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the item is retrieved",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.the_item_is_retrieved()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Trying to retrieve an item using an invalid id",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user has an invalid id",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.ItemsSteps.the_user_has_an_invalid_id()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request to retrieve the item is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_retrieve_the_item_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 404 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a \"Oject with id\u003d%s was not found.\" error will be displayed",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.a_error_will_be_displayed(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Ability to list of items",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the request to retrieve all items is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_retrieve_all_items_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 200 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "13 items will be returned",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.items_will_be_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the get list of items response is validated",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.the_get_list_of_items_response_is_validated()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Successfully delete an item",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "an item has been added",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.ItemsSteps.an_item_has_been_added()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request to delete the item is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_delete_the_item_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 200 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a \"Object with id \u003d %s has been deleted.\" message will be displayed",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.a_message_will_be_displayed(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Trying to delete an item using an invalid id",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@parallel"
    },
    {
      "name": "@api"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "the user has an invalid id",
  "keyword": "Given "
});
formatter.match({
  "location": "steps.ItemsSteps.the_user_has_an_invalid_id()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "the request to delete the item is made",
  "keyword": "When "
});
formatter.match({
  "location": "steps.ItemsSteps.the_request_to_delete_the_item_is_made()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a 404 response code is returned",
  "keyword": "Then "
});
formatter.match({
  "location": "steps.ItemsSteps.a_response_code_is_returned(int)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "a \"Object with id \u003d %s doesn\u0027t exist.\" error will be displayed",
  "keyword": "And "
});
formatter.match({
  "location": "steps.ItemsSteps.a_error_will_be_displayed(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});