import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        features = {"/Users/timothyloukas/Desktop/Project/3/restful-api/src/test/resources/features/Items.feature:24"},
        plugin = {"json:/Users/timothyloukas/Desktop/Project/3/restful-api/target/cucumber-parallel/4.json"},
        monochrome = true,
                glue = {"steps"})

public class Runner004 {
}
