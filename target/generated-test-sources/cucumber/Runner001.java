import org.junit.runner.RunWith;
import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        strict = true,
        features = {"/Users/timothyloukas/Desktop/Project/3/restful-api/src/test/resources/features/Items.feature:4"},
        plugin = {"json:/Users/timothyloukas/Desktop/Project/3/restful-api/target/cucumber-parallel/1.json"},
        monochrome = true,
                glue = {"steps"})

public class Runner001 {
}
