export BROWSER_PROFILE=${BROWSER_PROFILE:-'Headless'}
export TEST_TAGS=${TEST_TAGS:-'@parallel and not @bug and not @exploratory'}

### Execute Tests ###
mvn clean install -Dcucumber.options="--tags '${TEST_TAGS}'" 