package helpers;

import static io.restassured.RestAssured.given;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import api.APIObject;
import datagenerators.ItemBuilder;
import dataobjects.Item;
import io.restassured.http.ContentType;
import steps.TestBase;

public class ApiHelper extends TestBase {

	/**
	 * Helper method to create an item
	 * 
	 * @param name  The name of the item that user wants to create
	 * @param price The price of the item that user wants to create
	 * 
	 * @return an Item object
	 * 
	 * @throws JsonProcessingException
	 */
	public static Item createItem(String name, double price) throws JsonProcessingException {
		Item item = ItemBuilder.getItem().withName(name).withData("price", price).build();

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);

		//@formatter:off
		return given()
				  .spec(requestSpec)
				  .contentType(ContentType.JSON)
				  .body(mapper.writeValueAsString(item))
			  .when()
			  		.log().all()
				  .post(APIObject.getCreateObjectURL())
			  .then()
				  .assertThat().statusCode(200)
				  .extract().body().as(Item.class);
		//@formatter:on
	}

	/**
	 * Helper method to Delete an item
	 * 
	 * @param id The id of the item that the user wants to delete
	 */
	public void deleteItem(String id) {
		//@formatter:off
		given()
			  .spec(requestSpec)
			  .contentType(ContentType.JSON)
		   .when()
			  .delete(APIObject.getDeleteObjectURL(id))
		   .then()
			  .assertThat().statusCode(200);
		//@formatter:on
	}

}
