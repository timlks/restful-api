package configuration;

import java.io.File;
import java.util.HashMap;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

/**
 * 
 * @author timothyloukas
 *
 *         Environment Configuration class that reads variables from the
 *         environments.conf file
 */
public class EnvironmentConfig extends ConfigurationManager {

	private Config config;
	private String baseUrl;

	private HashMap<String, File> configFiles;

	public EnvironmentConfig() {
		configFiles = getConifgFiles();
		loadConfigFile();
		loadConfiguration();
	}

	public void loadConfigFile() {
		config = ConfigFactory.parseFile(configFiles.get("environments")).getConfig("vdiLocal").resolve();
	}

	@Override
	public void loadConfiguration() {
		baseUrl = config.getString("base-url");
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

}