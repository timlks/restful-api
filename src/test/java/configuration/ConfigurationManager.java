package configuration;

import java.io.File;
import java.util.HashMap;

public abstract class ConfigurationManager {

	public abstract void loadConfiguration();
	
	public HashMap<String, File> getConifgFiles() {
		try {
			HashMap<String, File> files = new HashMap<>();
			files.put("environments", new File(
					Thread.currentThread().getContextClassLoader().getResource("Config/environments.conf").getFile()));

			return files;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
