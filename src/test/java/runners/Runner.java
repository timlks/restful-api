package runners;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

// @formatter:off
	@RunWith(CucumberWithSerenity.class)
	@CucumberOptions(
			features = "src/test/resources/features", 
			glue = { "steps" }, 
			tags = { "@api and not @exploratory and not @bug" },
			plugin = { "pretty", "html:target/test-reports", "json:target/cucumber-report/cucumber-google-test.json" })
// @formatter:on

/**
 * 
 * @author timothyloukas
 *
 *         Test Runner
 */
public class Runner {

}
