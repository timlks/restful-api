package session;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import configuration.EnvironmentConfig;

/**
 * 
 * @author Timothy Lks
 *
 */
@Component
public class Session {

	private Map<String, Object> sharedData;

	private EnvironmentConfig configuration;

	public Session() {
		configuration = new EnvironmentConfig();
		sharedData = new HashMap<>();
		setUpData();
	}

	/**
	 * @return the BASE URL
	 */
	public String getBaseUrl() {
		return configuration.getBaseUrl();
	}

	public String getData(String key) {
		return sharedData.get(key).toString();
	}

	public void setData(String key, String value) {
		sharedData.put(key, value);
	}

	public void setUpData() {
		sharedData.put("newItem", "false");
	}

}
