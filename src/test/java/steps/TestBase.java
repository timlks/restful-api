package steps;

import static io.restassured.RestAssured.config;
import static io.restassured.config.EncoderConfig.encoderConfig;
import static net.serenitybdd.rest.SerenityRest.given;
import static org.junit.Assert.fail;

import java.net.MalformedURLException;

import org.springframework.beans.factory.annotation.Autowired;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import session.Session;
import session.SessionSingleton;

public class TestBase {

	@Autowired
	protected static Session session;
	protected static RequestSpecification requestSpec;
	protected static Response response;
	protected static ValidatableResponse json;
	protected static RequestSpecification request;

	public static void StepsInit() {
		try {
			session = SessionSingleton.getSession();
		} catch (MalformedURLException e) {
			e.printStackTrace();
			fail("Unable to initialise session: " + e.getMessage());
		}
	}

	public static void initialise() {
		StepsInit();

		requestSpec = createDefaultRequestSpec(session.getBaseUrl());
		request = given().spec(requestSpec).contentType(ContentType.JSON);
	}

	/**
	 * Builds the request specification
	 * 
	 * @return the request specification
	 */
	private static RequestSpecification createDefaultRequestSpec(String baseUrl) {
		RestAssured.config = config()
				.encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false));

		return new RequestSpecBuilder().setBaseUri(baseUrl).build();
	}

}
