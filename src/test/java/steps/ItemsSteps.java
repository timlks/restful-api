package steps;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import api.APIObject;
import datagenerators.RandomValuesGenerator;
import dataobjects.ErrorResponse;
import dataobjects.Item;
import dataobjects.SuccessResponse;
import helpers.ApiHelper;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.module.jsv.JsonSchemaValidator;
import net.thucydides.core.annotations.Steps;

public class ItemsSteps extends TestBase {

	private Item item;
	private Map<String, Object> data = new HashMap<>();
	private ErrorResponse errorResponse;
	private SuccessResponse successResponse;

	private String id;

	@Steps
	private ApiHelper apiHelper;

	@Before
	public void setUp() {
		initialise();
	}

	@After
	public void tearDown() {
		if (session.getData("newItem").equals("true")) {
			apiHelper.deleteItem(item.getId());
		}
		session.setData("newItem", "false");
	}

	@Given("a {string} item is created")
	public void a_item_is_created(String name) {
		item = new Item();
		item.setName(name);
	}

	@Given("the CPU model is {string}")
	public void the_CPU_model_is(String cpuModel) {
		data.put("CPU model", cpuModel);
	}

	@Given("has a price of {string}")
	public void has_a_price_of(String price) {
		data.put("Price", price);
	}

	@Given("the user has an invalid id")
	public void the_user_has_an_invalid_id() {
		id = "invalid";
	}

	@Given("an item has been added")
	public void an_item_has_been_added() throws JsonProcessingException {
		item = ApiHelper.createItem(RandomValuesGenerator.randomString(5), 5);
		id = item.getId();
		session.setData("newItem", "true");
	}

	@When("the request to add the item is made")
	public void the_request_to_add_the_item_is_made() throws JsonProcessingException {
		item.setData(data);

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);

		request.body(mapper.writeValueAsString(item));
		response = request.when().post(APIObject.getCreateObjectURL());
	}

	@When("the request to delete the item is made")
	public void the_request_to_delete_the_item_is_made() {
		response = request.when().delete(APIObject.getDeleteObjectURL(id));
	}

	@When("the request to retrieve the item is made")
	public void the_request_to_retrieve_the_item_is_made() {
		response = request.when().get(APIObject.getRetrieveObjectURL(id));
	}

	@When("the request to retrieve all items is made")
	public void the_request_to_retrieve_all_items_is_made() {
		request.log().all();
		response = request.when().get(APIObject.getRetrieveObjectsListURL());
	}

	@Then("the item is retrieved")
	public void the_item_is_retrieved() {
		Item actualItem = json.extract().body().as(Item.class);
		assertThat(actualItem.getId()).isNotEmpty();
		assertThat(actualItem.getName()).isEqualTo(item.getName());
		assertThat(actualItem.getData()).isEqualTo(item.getData());
	}

	@Then("a {int} response code is returned")
	public void a_response_code_is_returned(int responseCode) {
		json = response.then().statusCode(responseCode);
	}

	@Then("a {string} is created")
	public void a_is_created(String name) {
		item = json.extract().body().as(Item.class);

		assertThat(item.getName()).isEqualTo(name);
		assertThat(item.getId()).isNotEmpty();
		assertThat(item.getCreatedAt()).isNotEmpty();

		session.setData("newItem", "false");
	}

	@Then("{int} items will be returned")
	public void items_will_be_returned(int noItems) {
		Item[] items = json.extract().body().as(Item[].class);
		assertThat(items.length).isEqualTo(noItems);
	}

	@Then("the get list of items response is validated")
	public void the_get_list_of_items_response_is_validated() {
		response.then().assertThat()
				.body(JsonSchemaValidator.matchesJsonSchemaInClasspath("schemas/getItemsList.json"));
	}

	@Then("a {string} message will be displayed")
	public void a_message_will_be_displayed(String message) {
		successResponse = json.extract().body().as(SuccessResponse.class);
		assertThat(String.format(message, id)).isEqualTo(successResponse.getMessage());
		session.setData("newItem", "false");
	}

	@Then("a {string} error will be displayed")
	public void a_error_will_be_displayed(String error) {
		errorResponse = json.extract().body().as(ErrorResponse.class);
		assertThat(String.format(error, id)).isEqualTo(errorResponse.getError());
	}
}
