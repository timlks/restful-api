@parallel @api
Feature: Testing the items

  Scenario: Successfully adding an item
    Given a "Apple MacBook Pro 16" item is created
    And the CPU model is "Intel Core i9"
    And has a price of "1849.99"
    When the request to add the item is made
    Then a 200 response code is returned
    And a "Apple MacBook Pro 16" is created

  Scenario: Successfully retrieving an item
    Given an item has been added
    When the request to retrieve the item is made
    Then a 200 response code is returned
    And the item is retrieved

  Scenario: Trying to retrieve an item using an invalid id
    Given the user has an invalid id
    When the request to retrieve the item is made
    Then a 404 response code is returned
    And a "Oject with id=%s was not found." error will be displayed

  Scenario: Ability to list of items
    When the request to retrieve all items is made
    Then a 200 response code is returned
    And 13 items will be returned
    And the get list of items response is validated

  Scenario: Successfully delete an item
    Given an item has been added
    When the request to delete the item is made
    Then a 200 response code is returned
    And a "Object with id = %s has been deleted." message will be displayed

  Scenario: Trying to delete an item using an invalid id
    Given the user has an invalid id
    When the request to delete the item is made
    Then a 404 response code is returned
    And a "Object with id = %s doesn't exist." error will be displayed
