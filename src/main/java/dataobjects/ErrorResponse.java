package dataobjects;

import lombok.Data;

@Data
public class ErrorResponse {

	private String error;

	/**
	 * 
	 */
	public ErrorResponse() {
		super();
	}

}
