package dataobjects;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Item {

	private String id;
	private String name;
	private Map<String, Object> data = new HashMap<>();
	private String createdAt;

	/**
	 * 
	 */
	public Item() {
		super();
	}

	public Item(String name, Map<String, Object> data) {
		super();
		this.name = name;
		this.data = data;
	}

}
