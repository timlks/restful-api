package dataobjects;

import lombok.Data;

@Data
public class SuccessResponse {

	private String message;

	/**
	 * 
	 */
	public SuccessResponse() {
		super();
	}

}
