package datagenerators;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * 
 * @author timothyloukas
 *
 *         Contains methods that help generating random values
 */
public class RandomValuesGenerator {

	/**
	 * Creates a random generated String
	 * 
	 * @param numberOfChars The number of characters that the string will be
	 *                      consisted of
	 * 
	 * @return a random String
	 */
	public static String randomString(int numberOfChars) {
		return RandomStringUtils.randomAlphanumeric(randomInt(numberOfChars, numberOfChars)).toLowerCase();
	}

	/**
	 * Creates a random generated int
	 * 
	 * @param min The minimum int that the user want to use
	 * @param max The maximum int that the user want to use
	 * 
	 * @return a random int
	 */
	public static int randomInt(int min, int max) {
		return new Random().nextInt(max + 1 - min) + min;
	}

}