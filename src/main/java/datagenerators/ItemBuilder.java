package datagenerators;

import java.util.HashMap;
import java.util.Map;

import dataobjects.Item;

public class ItemBuilder {

	public String name;
	public Map<String, Object> data = new HashMap<>();

	public static ItemBuilder getItem() {
		return new ItemBuilder();
	}

	public ItemBuilder withName(String name) {
		this.name = name;
		return this;
	}

	public ItemBuilder withData(String name, Object value) {
		data.put(name, value);
		return this;
	}

	public Item build() {
		return new Item(name, data);
	}
}
