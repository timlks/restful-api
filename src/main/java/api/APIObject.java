package api;

public class APIObject {

	/**
	 * Provides the URL to create an object
	 * 
	 * @return the URL to create an object
	 */
	public static String getCreateObjectURL() {
		return "/objects";
	}

	/**
	 * Provides the URL to retrieve an object
	 * 
	 * @param objectId The id of the object that the user wants to get
	 * 
	 * @return the URL to retrieve an object
	 */
	public static String getRetrieveObjectURL(String objectId) {
		return String.format("/objects/%s", objectId);
	}

	/**
	 * Provides the URL to retrieve a list of objects
	 * 
	 * @return the URL to retrieve a list of objects
	 */
	public static String getRetrieveObjectsListURL() {
		return "/objects";
	}

	/**
	 * Provides the URL to delete an object
	 * 
	 * @param objectId The id of the object that the user wants to delete
	 * 
	 * @return the URL to delete an object
	 */
	public static String getDeleteObjectURL(String objectId) {
		return String.format("/objects/%s", objectId);
	}

}
